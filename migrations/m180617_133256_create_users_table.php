<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180617_133256_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'login' => $this->string(100)->notNull()->unique(),
            'password' => $this->string(256)->notNull(),
            'nick_name' => $this->string(256)->defaultValue(''),
            'page_name' => $this->string(256)->defaultValue('')->unique(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW()',
            'updated_at' => 'timestamp default current_timestamp on update current_timestamp',
            'banned_at' => $this->timestamp()->defaultValue(null),
            'everlasting_ban' => $this->boolean()->defaultValue(null)
        ]);

        $this->createIndex(
            'idx-users-login-password',
            'users',
            ['login','password'],
            true
        );

        $this->createIndex(
            'idx-users-page_name',
            'users',
            'page_name',
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-users-login-password', 'users');
        $this->dropIndex('idx-users-page_name', 'users');

        $this->dropTable('users');
    }
}
