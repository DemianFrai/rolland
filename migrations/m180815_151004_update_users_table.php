<?php

use yii\db\Migration;

/**
 * Class m180815_151004_update_users_table
 */
class m180815_151004_update_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'name', $this->string(256)->defaultValue('')->after('id'));
        $this->addColumn('users', 'last_name', $this->string(256)->defaultValue('')->after('name'));
        $this->addColumn('users', 'logo', $this->string(256)->defaultValue('')->after('last_name'));


        $this->dropIndex('idx-users-login-password', 'users');

        $this->dropColumn('users', 'password');
        $this->addColumn('users', 'password_hash', $this->string(512)->after('login'));

        $this->createIndex(
            'idx-users-login-password',
            'users',
            ['login','password_hash'],
            false
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'name');
        $this->dropColumn('users', 'last_name');
        $this->dropColumn('users', 'logo');

        $this->dropIndex('idx-users-login-password', 'users');

        $this->dropColumn('users', 'password_hash');
        $this->addColumn('users', 'password', $this->string(256)->notNull()->after('login'));

        $this->createIndex(
            'idx-users-login-password',
            'users',
            ['login','password'],
            true
        );
    }
}
