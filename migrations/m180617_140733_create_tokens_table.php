<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tokens`.
 */
class m180617_140733_create_tokens_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tokens', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->defaultValue(null),
            'access_token' => $this->string(256)->notNull()->unique(),
            'refresh_token' => $this->string(256)->notNull()->unique(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW()',
            'updated_at' => 'timestamp default current_timestamp on update current_timestamp',
            'ended_at' => $this->timestamp()->defaultValue(null),
            'refresh_ended_at' => $this->timestamp()->defaultValue(null),
            'deleted_at' => $this->timestamp()->defaultValue(null),
        ]);

        $this->createIndex(
            'idx-tokens-access_token',
            'tokens',
            'access_token',
            true
        );

        $this->createIndex(
            'idx-tokens-refresh_token',
            'tokens',
            'refresh_token',
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-tokens-access_token', 'tokens');
        $this->dropIndex('idx-tokens-refresh_token', 'tokens');
        $this->dropTable('tokens');
    }
}
