<?php
/**
 * Created by PhpStorm.
 * User: DemianFrai
 * Date: 17.06.2018
 * Time: 18:37
 */

namespace app\modules\api\v1\base;


use yii\base\Module;

class BaseModule extends Module
{
    public function init()
    {
        parent::init();

    }
}