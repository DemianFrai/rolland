<?php

namespace app\modules\api\modules\v1\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "tokens".
 *
 * @property int $id
 * @property int $user_id
 * @property string $access_token
 * @property string $refresh_token
 * @property string $created_at
 * @property string $updated_at
 * @property string $ended_at
 */
class Token extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tokens';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['access_token', 'refresh_token'], 'required'],
            [['created_at', 'updated_at', 'ended_at'], 'safe'],
            [['access_token', 'refresh_token'], 'string', 'max' => 256],
            [['access_token'], 'unique'],
            [['refresh_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'access_token' => 'Access Token',
            'refresh_token' => 'Refresh Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'ended_at' => 'Ended At',
        ];
    }
}
