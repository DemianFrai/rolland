<?php

namespace app\modules\api\modules\v1\models;

use yii\base\BaseObject;
use yii\web\IdentityInterface;

class UserIdentity extends BaseObject implements IdentityInterface
{
    public $id;
//    public $username;
//    public $password;
//    public $authKey;
    public $accessToken;
    public $user;


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return User::findOne(['id', $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /**
         * @var $tokenModel Token|null
         */
        $date = (new \DateTime('now'))->format('Y-m-d H:i:s');
        $tokenModel = Token::find()->where(['access_token' => $token, 'deleted_at' => null])->andWhere(['>', 'ended_at', $date])->andWhere(['>', 'refresh_ended_at', $date])->one();
        if (!$tokenModel){
            return null;
        }

        if ($tokenModel->user_id){
            $user = User::findOne($tokenModel->user_id);
        }else{
            $user = new User();
        }
        $identity = new self();
        $identity->accessToken = $token;
        $identity->user = $user;
        $identity->id = $user->id;

        return $identity;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user =  User::findOne(['login', $username]);
        if ($user) {
            return new static($user);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
