<?php

namespace app\modules\api\modules\v1\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $nick_name
 * @property string $page_name
 * @property string $created_at
 * @property string $updated_at
 * @property string $banned_at
 * @property int $everlasting_ban
 * @property string password_hash
 */
class User extends ActiveRecord implements IdentityInterface
{
    const SALT = 'ngjhsdihsdjhsdg';
    public $authKey;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    public function setPassword($value){
        $this->password_hash = hash('sha512', $value.self::SALT);
    }

    public function getPassword(){
        return $this->password_hash;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login', 'password', 'name', 'last_name', 'nick_name', 'page_name'], 'required'],
            [['created_at', 'updated_at', 'banned_at', 'password'], 'safe'],
            [['everlasting_ban'], 'integer'],
            [['login'], 'string', 'max' => 100],
            [['nick_name', 'page_name', 'name', 'last_name', 'logo'], 'string', 'max' => 256],
            [['login'], 'unique'],
            [['page_name'], 'unique'],
        ];
    }

    public function fields()
    {
        return [
            'id','name','last_name','logo','nick_name','page_name'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'last_name' => 'Last Name',
            'login' => 'Login',
            'logo' => 'Logo',
            'password' => 'Password',
            'nick_name' => 'Nick Name',
            'page_name' => 'Page Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'banned_at' => 'Banned At',
            'everlasting_ban' => 'Everlasting Ban',
        ];
    }

    public function afterSave($insert, $changedAttributes){
        if ($insert){
            $token = Yii::$app->user->identity->getAuthKey();
            $tokenModel = Token::findOne(['access_token' => $token]);
            $tokenModel->user_id = $this->id;
            $tokenModel->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return User::findOne(['id', $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /**
         * @var $tokenModel Token|null
         */
        $date = (new \DateTime('now'))->format('Y-m-d H:i:s');
        $tokenModel = Token::find()->where(['access_token' => $token, 'deleted_at' => null])->andWhere(['>', 'ended_at', $date])->andWhere(['>', 'refresh_ended_at', $date])->one();
        if (!$tokenModel){
            return null;
        }

        if ($tokenModel->user_id){
            $user = self::findOne($tokenModel->user_id);
        }else{
            $user = new self();
        }
        $user->authKey = $token;

        return $user;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user =  self::findOne(['login', $username]);
        if ($user) {
            return new static($user);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password_hash === hash('sha512', $password.self::SALT);
    }
}
