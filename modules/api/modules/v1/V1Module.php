<?php

namespace app\modules\api\modules\v1;

use Yii;
use yii\base\BootstrapInterface;
use yii\base\Module;

/**
 * v1 module definition class
 */
class V1Module extends Module implements BootstrapInterface
{

    public $controllerNamespace = 'app\modules\api\modules\v1\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        Yii::$app->set('user', [
            'class' => 'yii\web\User',
            'identityClass' => 'app\modules\api\modules\v1\models\User',
            'enableAutoLogin' => false,
        ]);
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'api/v1/token',
                        'api/v1/login',
                        'api/v1/user',
                    ],
                    'pluralize' => false,
                    'tokens' => [
                        '{id}' => '<id:\w+>'
                    ]
                ]
            ]
        );
    }

}
