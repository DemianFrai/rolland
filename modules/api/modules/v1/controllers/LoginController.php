<?php
/**
 * Created by PhpStorm.
 * User: DemianFrai
 * Date: 17.06.2018
 * Time: 17:55
 */

namespace app\modules\api\modules\v1\controllers;

use app\modules\api\modules\v1\models\Token;
use app\modules\api\modules\v1\models\User;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Yii;

class LoginController extends BaseController
{

    public function actionIndex(){
        /** @noinspection PhpUndefinedFieldInspection */
        $user = Yii::$app->user->identity;
        /** @noinspection PhpUndefinedMethodInspection */
        return json_encode($user->toArray(), true);

    }

    public function actionCreate($login, $password){

        $user = User::findOne(['login' => $login]);
        if ($user && $user->validatePassword($password)){
            $token = Token::findOne(['access_token' => Yii::$app->user->identity->getAuthKey()]);
            $token->user_id = $user->id;
            $token->save();
            return json_encode($user->toArray(), true);
        }

        throw new AccessDeniedException('Access denied.', 403);

    }
}