<?php
/**
 * Created by PhpStorm.
 * User: DemianFrai
 * Date: 17.06.2018
 * Time: 17:57
 */

namespace app\modules\api\modules\v1\controllers;

use Yii;
use yii\db\ActiveRecord;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\BaseArrayHelper;
use yii\web\Controller;

class BaseController extends Controller
{
    /**
     * @var ActiveRecord::class
     */
    protected $modelClass;
    protected $customController = false;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::class;
        return $behaviors;
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function runAction($id, $params = [])
    {
        // Extract the params from the request and bind them to params
        $params = BaseArrayHelper::merge(Yii::$app->getRequest()->getBodyParams(), $params);
        return parent::runAction($id, $params);
    }

}