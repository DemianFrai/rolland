<?php
/**
 * Created by PhpStorm.
 * User: DemianFrai
 * Date: 17.06.2018
 * Time: 17:55
 */

namespace app\modules\api\modules\v1\controllers;

use app\modules\api\modules\v1\models\Token;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\UnauthorizedHttpException;

class TokenController extends BaseController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = ['create', 'update'];
        return $behaviors;
    }

    public function actionView($id){

        $token = Token::find()->where(['access_token' => $id])->one();

        if (!$token){
            throw new NotFoundHttpException('Page not found.');
        }

        return json_encode($token->toArray());

    }

    public function actionCreate(){

        $token = new Token([
            'access_token' => hash('sha256', 'access_token'.time().rand(0,65532)),
            'refresh_token' => hash('sha256', 'refresh_token'.time().rand(0,65532)),
            'ended_at' => (new \DateTime('now'))->add(date_interval_create_from_date_string('1 days')  )->format('Y-m-d H:i:s'),
            'refresh_ended_at' => (new \DateTime('now'))->add(date_interval_create_from_date_string('30 days')  )->format('Y-m-d H:i:s')
        ]);

        $token->save();

        return json_encode($token->toArray());
    }

    public function actionUpdate($id){
        Yii::$app->getRequest();
        $authHeader = Yii::$app->getRequest()->getHeaders()->get('Authorization');

        if (preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches)) {
            $accessToken = $matches[1];
        } else {
            throw new UnauthorizedHttpException('Your request was made with invalid credentials.');
        }

        $token = Token::find()->where(['access_token' => $accessToken, 'refresh_token' => $id])
            ->andWhere(['>', 'refresh_ended_at', (new \DateTime('now'))->format('Y-m-d H:i:s')])
            ->one();

        if (!$token){
            throw new UnauthorizedHttpException('Your request was made with invalid credentials.');
        }

        $token->setAttributes([
            'access_token' => hash('sha256', 'access_token'.time().rand(0,65532)),
            'refresh_token' => hash('sha256', 'refresh_token'.time().rand(0,65532)),
            'ended_at' => (new \DateTime('now'))->add(date_interval_create_from_date_string('1 days')  )->format('Y-m-d H:i:s'),
            'refresh_ended_at' => (new \DateTime('now'))->add(date_interval_create_from_date_string('30 days')  )->format('Y-m-d H:i:s')
        ]);

        $token->save();

        return json_encode($token->toArray());
    }

}