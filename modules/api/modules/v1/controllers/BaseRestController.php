<?php
/**
 * Created by PhpStorm.
 * User: DemianFrai
 * Date: 17.06.2018
 * Time: 17:57
 */

namespace app\modules\api\modules\v1\controllers;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UnprocessableEntityHttpException;

class BaseRestController extends BaseController
{

    /**
     * @return ActiveRecord
     * @throws UnprocessableEntityHttpException
     */
    private function createRecord(){
        if (!$this->modelClass){
            throw new UnprocessableEntityHttpException('Unprocessable Entity', 422);
        }

        $record = new $this->modelClass();

        if (!$record instanceof ActiveRecord){
            throw new UnprocessableEntityHttpException('Unprocessable Entity', 422);
        }

        return $record;
    }

    public function actionIndex($filter = ''){
        if ($filter){
            $filter = json_decode($filter, true);
        }

        $builder = $this->modelClass::find();
        if ($filter){
            $builder->where($filter);
        }
        $records = $builder->all();

        foreach ($records as &$record){
            $record = $record->toArray();
        }
        return json_encode($records);
    }

    public function actionCreate(){
        $record = $this->createRecord();
        $record->setAttributes(Yii::$app->getRequest()->post());
        if (!$record->validate()){
            Yii::$app->response->statusCode = 400;
            return json_encode($record->getErrors());
        }
        $record->save();
        return json_encode($record->toArray());
    }

    public function actionUpdate($id){
        $record = $this->modelClass::findOne(['id' => $id]);
        $record->setAttributes(Yii::$app->getRequest()->post());
        if (!$record->validate()){
            Yii::$app->response->statusCode = 400;
            return json_encode($record->getErrors());
        }
        $record->save();
        return json_encode($record->toArray());
    }

}