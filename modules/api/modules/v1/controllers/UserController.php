<?php
/**
 * Created by PhpStorm.
 * User: DemianFrai
 * Date: 17.06.2018
 * Time: 17:55
 */

namespace app\modules\api\modules\v1\controllers;

use app\modules\api\modules\v1\models\User;

class UserController extends BaseRestController
{
    protected $modelClass = User::class;
}