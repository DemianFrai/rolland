<?php

namespace app\modules\api;

use Yii;
use yii\base\Module;

/**
 * v1 module definition class
 */
class ApiModule extends Module
{


    public function init()/**
     * {@inheritdoc}
     */
    {
        parent::init();

        $this->modules = [
            'v1' => [
                'class' => 'app\modules\api\modules\v1\V1Module',
            ],
        ];
    }

    public function beforeAction($action)
    {

        $handler = new ApiErrorHandler();
        Yii::$app->set('errorHandler', $handler);
        //необходимо вызывать register, это обязательный метод для регистрации обработчика
        $handler->register();

        return parent::beforeAction($action);
    }
}
