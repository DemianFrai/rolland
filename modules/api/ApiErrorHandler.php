<?php
/**
 * Created by PhpStorm.
 * User: DemianFrai
 * Date: 21.06.2018
 * Time: 21:41
 */

namespace app\modules\api;


use Yii;
use yii\web\ErrorHandler;
use yii\web\Response;

class ApiErrorHandler extends ErrorHandler
{

    const ERROR_CODES=[
        'Your request was made with invalid credentials.' => 401,
        'Page not found.' => 404,
    ];
    /**
     * @inheridoc
     * @param \Error|\Exception $exception
     */

    protected function renderException($exception)
{
    if (Yii::$app->has('response')) {
        $response = Yii::$app->getResponse();
    } else {
        $response = new Response();
    }

    $message = $exception->getMessage();
    $response->data = $message;

    if ($exception->getCode()){
        $response->setStatusCode($exception->getCode());
    }elseif(isset(self::ERROR_CODES[$message])){
        $response->setStatusCode(self::ERROR_CODES[$message]);
    }


    $response->send();
}

    /**
     * @inheritdoc
     */

    protected function convertExceptionToArray($exception)
{
    return [
        'meta'=>
            [
                'status'=>'error',
                'errors'=>[
                    ['message'=>$exception->getMessage(),'code'=>$exception->getCode()]
                ]
            ]
    ];
}
}