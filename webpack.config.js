/**
 * Created by DemianFrai on 10.09.2017.
 */

const webpack = require("webpack");
const path = require("path");

//noinspection JSUnresolvedFunction
module.exports = {
    mode: 'development',
    entry: path.resolve(__dirname, "dist/app.js"),
    output: {
        path: path.resolve(__dirname, "web/js"),
        publicPath: path.resolve(__dirname, "web/js"),
        filename: "rolland.common.js"
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000
    },
    module: {
        rules: [
            {
                loader: "vue-loader",
                test: /\.vue$/
            }
        ]
    },
    resolve:{
      alias: {
          "vue": "vue/dist/vue.common.js",
      }
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            vue: {
                loaders: {
                    js: "babel-loader",
                    // I"d like to do this
                    options: {
                        presets: ["es2015", "stage-2"],
                        plugins: ["transform-runtime"],
                        cacheDirectory: false
                    }
                }
            }
        }),
    ]
};