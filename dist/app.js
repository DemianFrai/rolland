/**
 * Created by DemianFrai on 10.09.2017.
 */
// "use strict";


import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import Toastr from 'vue-toastr';
import RollandPlugin from './plugins/RollandPlugin.js'


// import toastr scss file: need webpack sass-loader
// require('../node_modules/vue-toastr/src/vue-toastr.scss');

import App from './Components/App.vue'
import config from './config/config.js';

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Toastr);
Vue.use(RollandPlugin);




Vue.http.options.emulateJSON = true;

Vue.http.interceptors.push(
    (request, next)  => {

        next((response) => {
            if (response.status === 401) {

                Vue.http.put(config.apiUrl+'token/'+window.localStorage.rolland_refresh_token,{},{headers:{Authorization: 'Bearer ' + window.localStorage.rolland_token}}).then(
                    response => {
                        if (!response || !response.data || !response.data.access_token || !response.data.refresh_token){
                            window.localStorage.setItem('rolland_token', '');
                            window.localStorage.setItem('rolland_refresh_token', '');
                            window.location.reload();
                        } else{
                            window.localStorage.setItem('rolland_token', response.data.access_token);
                            window.localStorage.setItem('rolland_refresh_token', response.data.refresh_token);
                            request.headers.map.Authorization[0] = 'Bearer '+ response.data.access_token;
                            return Vue.http(request).then((data) => {
                                return data
                            });
                        }
                    },
                    () => {
                        window.localStorage.setItem('rolland_token', '');
                        window.localStorage.setItem('rolland_refresh_token', '');
                        window.location.reload();
                    },
                );
            }
        })

    });

new Vue(App).$mount('#rolland-application');



