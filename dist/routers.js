//Base Pages
import BasePage from './Components/Pages/BasePage.vue';
import SettingsPage from './Components/Pages/SettingsPage.vue';
import RegistrationPage from './Components/Pages/RegistrationPage.vue';
import UserPage from './Components/Pages/UserPage.vue';

//SettingsPageModules
import CommonSettings from './Components/PageModules/Settings/CommonSettings.vue';
import ContactSettings from './Components/PageModules/Settings/ContactSettings.vue';

export default router => {
    router.addRoutes([
        {
            path: '/user/:pageName',
            name: 'user',
            component: resolve => resolve(UserPage),
        },
        {
            path: '/settings',
            name: 'settings',
            component: resolve => resolve(SettingsPage),
            redirect: '/settings/common',
            children: [
                {
                    path: 'common',
                    name: 'commonSettings',
                    component: resolve => resolve(CommonSettings),
                },
                {
                    path: 'contacts',
                    name: 'contactSettings',
                    component: resolve => resolve(ContactSettings),
                }
            ]
        },
        {
            path: '/registration',
            name: 'registration',
            component: resolve => resolve(RegistrationPage),
        },
        {
            path: '*',
            name: 'base',
            component: resolve => resolve(BasePage),
        },

        // '*': {
        //     component: resolve => resolve(notFound),
        // },
    ]);
};
