import config from '../config/config.js';

const RollandPlugin = {
    install(Vue){
        Vue.prototype.$config = config;

        Vue.prototype.$getAuthHeader = function(options){
            if (window.localStorage.rolland_token){
                if (!options.headers){
                    options.headers = {};
                }
                options.headers.Authorization = 'Bearer ' + window.localStorage.rolland_token;
            }
            return options;
        };

        Vue.prototype.$get = function(method, params = {}, options = {}){
            options = this.$getAuthHeader(options);

            let paramRow = '';
            let url = this.$config.apiUrl+method;

            for (let key in params) if (params.hasOwnProperty(key)){

                if (typeof params[key] === 'object'){
                    params[key] = JSON.stringify(params[key]);
                }
                let stringParam = key+'='+encodeURI(params[key]);
                paramRow += paramRow ? '&'+stringParam : stringParam;
            }
            if (paramRow){
                url = url +'?'+paramRow;
            }

            return this.$http.get(url, options);
        };

        Vue.prototype.$post = function(method, params = {}, options = {}){
            options = this.$getAuthHeader(options);
            return this.$http.post(this.$config.apiUrl+method, params, options);
        };

        Vue.prototype.$put = function(method, params = {}, options = {}){
            options = this.$getAuthHeader(options);
            return this.$http.put(this.$config.apiUrl+method, params, options);
        };

        Vue.prototype.$delete = function(method, params = {}, options = {}){
            options = this.$getAuthHeader(options);
            return this.$http.delete(this.$config.apiUrl+method, params, options);
        };

        Vue.prototype.$request = function (method, params = {}, type = 'get', options={}){
            if (['get', 'post', 'put', 'delete'].indexOf(type) === -1){
                type = 'get'
            }

            if (window.localStorage.rolland_token){
                if (!options.headers){
                    options.headers = {};
                }
                options.headers.Authorization = 'Bearer ' + window.localStorage.rolland_token;
            }

            let url = this.$config.apiUrl+method;

            if (type === 'get'){
                let paramRow = '';
                for (let key in params) if (params.hasOwnProperty(key)){
                    if (typeof params[key] === 'object'){
                        params[key] = JSON.stringify(params[key]);
                    }
                    let stringParam = key+'='+encodeURI(params[key]);
                    paramRow += paramRow ? '&'+stringParam : stringParam;
                }
                if (paramRow){
                    url = url +'?'+paramRow;
                }
                return this.$http.get(url,options);
            }else{
                return this.$http[type](url,params,options);
            }

        };
    }
};

export default RollandPlugin
