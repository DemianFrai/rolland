import Vue from 'vue';
import Vuex from 'vuex';
import mutations from './mutations.js';

Vue.use(Vuex);

const state = {
    init:false,
    isGuest:true,
    user: {}
};

export default new Vuex.Store({
    state,
    mutations,
});

