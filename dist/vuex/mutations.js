
export default {
    'init' : (state, data) => {
        if (data){
            state.user = data;
            state.isGuest = false;
        }
        state.init=true;
    },
};
