<?php

namespace app\assets;

use yii\web\AssetBundle;

class BootstrapAsset extends AssetBundle
{

    public $sourcePath = '@app/node_modules/mdbootstrap';
    public $css = [
        'css/bootstrap.min.css',
        'css/mdb.min.css',
    ];
    public $js = [
        'js/jquery-3.3.1.min.js',
        'js/popper.min.js',
        'js/bootstrap.min.js',
        'js/mdb.min.js',
    ];
}
