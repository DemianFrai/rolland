<?php

namespace app\assets;

use yii\web\AssetBundle;

class FontAwesome extends AssetBundle
{

    public $sourcePath = '@app/node_modules/font-awesome';
    public $css = [
        'css/font-awesome.min.css',
    ];
    public $js = [
    ];
}
